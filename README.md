> [!NOTE]
> **This repository is no longer maintained. Development of this project will continue on [Codeberg](https://codeberg.org/jcheatum/godot-template).**

# Godot Template

This project is a barebones template for games built in Godot. It contains much
of the UI boilerplate code that is common to nearly every game, with almost no
frills or customization, and provides a solid foundation on which to build your
projects.

## Project Structure

```
.
├── assets
│   ├── art
│   └── audio
│       ├── music
│       └── sfx
├── build
│   └── web
├── scenes
└── scripts
    └── global
```
- Assets (e.g. images, audio, video, 3D models, etc.) go in `res://assets/`.
I've created `art/` and `audio/{music,sfx}/` subdirectories, which I typically
use and recommend, but feel free to organize things however you want in here for
your own games.
- The `build/` directory is where builds will be exported to (this is already
configured in `export_presets.cfg`). All of the files for the web export will be
placed in `build/web/`, and will need to be zipped manually. **Make sure that
you zip the *contents* of the web folder and not the folder itself!** The 
`index.html` file needs to be at the top level of the zip in order for the web
build to embed properly on most sites.
- Scene files (.tscn) go in the `scenes/` folder. This is different from 
previous versions of this template, which had these in `assets/`.
- Scripts go in the `scripts/` folder.

### Scenes
- **Main** - Top-level manager node which orchestrates all of the primary scenes
and controls which one is shown to the player. You most likely won't interact
with this very much when building your game, since it doesn't (and shouldn't)
contain any statically instantiated nodes/scenes, instead loading and unloading
things dynamically.
- **MainMenu** - The main menu of the game. This is the first thing that will
be displayed when the game starts.
- **Game** - This is where you will build your actual gameplay. It gets 
instantiated when you press "Start" from the main menu.
- **SettingsMenu** - Basic settings menu that allows you to adjust volume and
fullscreen. It is displayed upon pressing the "Settings" button from either
the main menu or the pause menu. Upon pressing "Done" you will return to
whichever menu you came from.
- **PauseMenu** - Displayed when the game is paused. By default, pressing Esc
on the keyboard, or Start/+/Menu on a controller will pause the game.
- **MusicPlayer** - An extension of `AudioStreamPlayer` which automatically sets
the volume when changed via the Music Volume slider in the settings menu.
- **SfxPlayer2D** - An extension of `AudioStreamPlayer2D` which automatically 
sets the volume when changed via the Sfx Volume slider in the settings menu.