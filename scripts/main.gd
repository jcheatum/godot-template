class_name Main extends Node

var current_scene: Node = null

func _ready() -> void:
	show_main_menu()

func show_main_menu(focus_settings: bool = false) -> void:
	current_scene = Scenes.MAIN_MENU.instantiate()
	current_scene.game_start.connect(_on_main_menu_game_start)
	current_scene.open_settings.connect(_on_main_menu_open_settings)
	add_child(current_scene)
	if focus_settings:
		current_scene.focus_settings()


func _on_main_menu_game_start():
	current_scene.queue_free()
	show_game()


func _on_main_menu_open_settings():
	current_scene.queue_free()
	show_settings()
		

func show_settings() -> void:
	current_scene = Scenes.SETTINGS_MENU.instantiate()
	current_scene.done.connect(_on_settings_menu_done)
	add_child(current_scene)


func _on_settings_menu_done() -> void:
	current_scene.queue_free()
	show_main_menu(true)


func show_game() -> void:
	current_scene = Scenes.GAME.instantiate()
	current_scene.return_to_menu.connect(_on_game_return_to_menu)
	add_child(current_scene)


func _on_game_return_to_menu() -> void:
	current_scene.queue_free()
	show_main_menu()
