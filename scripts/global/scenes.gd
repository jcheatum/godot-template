extends Node

const MAIN_MENU = preload("res://scenes/main_menu.tscn")
const GAME = preload("res://scenes/game.tscn")
const SETTINGS_MENU = preload("res://scenes/settings_menu.tscn")
const PAUSE_MENU = preload("res://scenes/pause_menu.tscn")