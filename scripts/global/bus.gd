extends Node

@warning_ignore("unused_signal") signal music_vol_set(volume: float)
@warning_ignore("unused_signal") signal sfx_vol_set(volume: float)