extends Node

var music_volume: float = 1
var sfx_volume: float = 1


func _ready() -> void:
    Bus.music_vol_set.connect(func(volume): music_volume = linear_to_db(volume))
    Bus.sfx_vol_set.connect(func(volume): sfx_volume = linear_to_db(volume))
    