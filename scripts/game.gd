class_name Game extends Node

signal return_to_menu

enum GameState {
	RUNNING,
	PAUSED
}

var current_overlay: Control = null
var state: GameState = GameState.RUNNING

func _ready() -> void:
	get_tree().paused = false


func _input(event):
	if event.is_action_pressed("pause"):
		match state:
			GameState.RUNNING:
				get_tree().paused = true
				state = GameState.PAUSED
				show_pause_menu()
			GameState.PAUSED:
				get_tree().paused = false
				state = GameState.RUNNING
				current_overlay.queue_free()


func show_pause_menu(focus_settings: bool = false) -> void:
	current_overlay = Scenes.PAUSE_MENU.instantiate()
	current_overlay.open_settings.connect(_on_pause_menu_open_settings)
	current_overlay.resume.connect(_on_pause_menu_resume)
	current_overlay.return_to_menu.connect(func(): return_to_menu.emit())
	add_child(current_overlay)
	if focus_settings:
		current_overlay.focus_settings()


func _on_pause_menu_open_settings() -> void:
	current_overlay.queue_free()
	show_settings()


func _on_pause_menu_resume() -> void:
	current_overlay.queue_free()
	get_tree().paused = false
	state = GameState.RUNNING


func show_settings() -> void:
	current_overlay = Scenes.SETTINGS_MENU.instantiate()
	current_overlay.done.connect(_on_settings_menu_done)
	add_child(current_overlay)


func _on_settings_menu_done() -> void:
	current_overlay.queue_free()
	show_pause_menu(true)
