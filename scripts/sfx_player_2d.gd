class_name SfxPlayer2D extends AudioStreamPlayer2D


func _ready() -> void:
	Bus.sfx_vol_set.connect(func(volume): self.volume_db = linear_to_db(volume))
	self.volume_db = linear_to_db(GlobalVars.sfx_volume)
