class_name MusicPlayer extends AudioStreamPlayer


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Bus.music_vol_set.connect(func(volume): self.volume_db = linear_to_db(volume))
	self.volume_db = linear_to_db(GlobalVars.music_volume)
